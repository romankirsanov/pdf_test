import os
from flask import Flask, request, send_file
from sign import sign
import json

app = Flask(__name__)
port = int(os.environ.get("PORT", 5000))


@app.route('/sign', methods=['POST'])
def sign_pdf():
    print('pushing')
    file = request.files["document"]
    options = (
        json.loads(request.form.to_dict()['options'])
        if 'options' in request.form.to_dict()
        else {}
    )
    signed = sign(file, options)
    return send_file(
        signed,
        as_attachment=True,
        attachment_filename='signed.pdf',
        mimetype='application/pdf',
    )


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=port)
