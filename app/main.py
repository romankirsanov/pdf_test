from sign import sign


def main():
    file = open("signme.pdf", "rb")
    options = {
        "translate": {
            "bottom": 650,
            "left": 450,
        }
    }
    signed = sign(file, options)
    with open("signed.pdf", 'wb') as new_file:
        new_file.write(signed.read())


main()
