from pathlib import Path

import PyPDF2
import io
import os
import tempfile
from pikepdf import Pdf

import datetime
from cryptography.hazmat import backends
from cryptography.hazmat.primitives.serialization import pkcs12

from endesive.pdf import cms

ROOT = ''
TEMP_FILE_NAME = "corrupted-fixed.pdf"

date = datetime.datetime.utcnow() - datetime.timedelta(hours=12)
date = date.strftime("D:%Y%m%d%H%M%S+00'00'")
default_options = {
    "aligned": 0,
    "sigflags": 3,
    "sigflagsft": 132,
    "sigpage": 0,
    "sigbutton": True,
    "sigfield": "Signature1",
    "sigandcertify": True,
    "location": "ISR",
    "contact": "mail@mail",
    "signingdate": date,
    "reason": "Security",
    "password": "1234",
    "translate": {
        "bottom": 750,
        "left": 490,
    },
    "signScale": 0.65,
}


def stamp(pdf_file, options):
    with open(os.path.join(ROOT, "stamp.pdf"), "rb") as overlay:
        pdf = Pdf.open(pdf_file)
        pdf.save(TEMP_FILE_NAME)
        with open(os.path.join(ROOT, TEMP_FILE_NAME), "rb") as fixed:
            original = PyPDF2.PdfFileReader(fixed)
            foreground = PyPDF2.PdfFileReader(overlay).getPage(0)

            # add all pages to a writer
            writer = PyPDF2.PdfFileWriter()
            for i in range(original.getNumPages()):
                background = original.getPage(i)

                # merge the first two pages
                if i == 0:
                    translate = options['translate']
                    background.mergeScaledTranslatedPage(
                        foreground,
                        options['signScale'],
                        translate['left'],
                        translate['bottom'],
                    )

                writer.addPage(background)

            # write everything in the writer to a file
            temp_stamped = tempfile.NamedTemporaryFile(delete=False)
            with temp_stamped as outFile:
                writer.write(outFile)

            return temp_stamped


def sign(pdf_file, options=None):
    if options is None:
        options = {}

    conf = {**default_options, **options}

    temp_stamped = stamp(pdf_file, conf)

    with open(os.path.join(ROOT, "cer.p12"), "rb") as fp:
        p12 = pkcs12.load_key_and_certificates(
            fp.read(), b"1234", backends.default_backend()
        )

    datau = open(temp_stamped.name, "rb").read()
    datas = cms.sign(datau, conf, p12[0], p12[1], p12[2], "sha256")
    temp_signed = tempfile.NamedTemporaryFile(delete=False)
    with temp_signed as fp:
        fp.write(datau)
        fp.write(datas)

    with open(temp_signed.name, "rb") as fin:
        data = io.BytesIO(fin.read())

    os.remove(temp_stamped.name)
    os.remove(temp_signed.name)
    os.remove(TEMP_FILE_NAME)

    return data
